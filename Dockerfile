FROM mxklb/ola-server:1.0.4

RUN apt-get update && apt-get install -y python3-pip
RUN pip3 install --upgrade pip setuptools
RUN pip3 install paho-mqtt colour orjson
